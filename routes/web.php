<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



//Auth::routes();
Route::get('/login', 'LoginController@index')->name('login');
Route::post('/logout', 'LoginController@logout')->name('logout');
//line login
Route::get('/login/line', 'LoginController@redirectToProvider')->name('LineLogin');
Route::get('/login/line/callback', 'LoginController@handleProviderCallback')->name('LineCallback');

Route::get('/', 'StoreController@index')->name('home');

Route::middleware(['auth'])->group(function(){    
    Route::post('/', 'StoreController@store')->name('store.insert');
});
