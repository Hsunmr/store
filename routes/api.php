<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('', 'API\StoreAPIController@index')->name('api.index');
Route::post('', 'API\StoreAPIController@store')->name('api.store');
Route::get('{id}', 'API\StoreAPIController@show')->name('api.show');
Route::put('{id}', 'API\StoreAPIController@update')->name('api.update');
Route::delete('{id}', 'API\StoreAPIController@destroy')->name('api.destroy');
