<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Store;

class StoreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $stores = Store::all();
        return view('store',compact('stores'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         //validation
         $request->validate([
            'store_name'=> ['required','string'],
            'store_address'=>['required','string'],
            'store_phone' =>['required','string'],
            'store_owner' =>['required','string']
        ]);
        //insert new store
        Store::create([
            'store_id'=> Str::random(8),
            'store_name' =>  $request->input('store_name'),
            'store_address' =>  $request->input('store_address'),
            'store_phone' =>  $request->input('store_phone'),
            'store_owner' =>  $request->input('store_owner')     
        ]);
        return redirect()->route('home')
                         ->with('success', '店家新增成功');
    }

}
