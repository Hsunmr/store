<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

use App\User;

use Socialite;
use Auth;

class LoginController extends Controller
{
    public function index(){
        return view('login');
    }
     /**
     * Redirect the user to the GitHub authentication page.
     *
     * @return \Illuminate\Http\Response
     */
    public function redirectToProvider()
    {
        return Socialite::driver('line')->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback()
    {
        $lineuser = Socialite::driver('line')->user();  //ger profile

        $user = User::where('line_id',$lineuser->id)->first(); 
        
        //if not exist add user to users table
        if(!$user){
            $user= User::create([
                'line_id'=>$lineuser->id,
                'name'=>$lineuser->name,
                'picture'=>$lineuser->user['picture'],
                'password'=>Hash::make(Str::random(8))
            ]);
        }
        //login the user
        Auth::login($user);
        //back to home
        return redirect('/');
    }
    public function logout(Request $request)
    {
        $this->guard()->logout();

        $request->session()->invalidate();

        return redirect('/');
    }
    protected function guard()
    {
        return Auth::guard();
    }
}
