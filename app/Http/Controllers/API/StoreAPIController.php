<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Helpers\APIHelpers;
use Illuminate\Support\Str;
use App\Http\Controllers\Controller;
use App\Store;


class StoreAPIController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $stores = Store::all();
        
        $response = APIHelpers::createAPIResponse(false,200,'',$stores);
        
        return response()->json($response,200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'store_name'=> ['required','string'],
            'store_address'=>['required','string'],
            'store_phone' =>['required','string'],
            'store_owner' =>['required','string']
        ]);

        $store_create = Store::create([
            'store_id'=> Str::random(8),
            'store_name' =>  $request->input('store_name'),
            'store_address' =>  $request->input('store_address'),
            'store_phone' =>  $request->input('store_phone'),
            'store_owner' =>  $request->input('store_owner')     
        ]);
        if($store_create){
            $response = APIHelpers::createAPIResponse(false,201,'store created successfully',null);
            return response()->json($response,201);
        }else{
            $response = APIHelpers::createAPIResponse(true,400,'store create failed',null);
            return response()->json($response,400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $store = Store::where('store_id',$id)->first();
        $response = APIHelpers::createAPIResponse(false,200,'',$store);
        return response()->json($response,200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $store = Store::where('store_id',$id)->first();

        $store->store_name = $request->store_name;
        $store->store_address = $request->store_address;
        $store->store_phone = $request->store_phone;
        $store->store_owner = $request->store_owner;
        $store_update = $store->save();
        if($store_update){
            $response = APIHelpers::createAPIResponse(false,200,'store updated successfully',null);
            return response()->json($response,200);
        }else{
            $response = APIHelpers::createAPIResponse(true,400,'store update failed',null);
            return response()->json($response,400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $store = Store::where('store_id',$id)->first();
        $store_delete= $store->delete();
        if($store_delete){
            $response = APIHelpers::createAPIResponse(false,200,'store deleted successfully',null);
            return response()->json($response,200);
        }else{
            $response = APIHelpers::createAPIResponse(true,400,'store delete failed',null);
            return response()->json($response,400);
        }
    }
}
