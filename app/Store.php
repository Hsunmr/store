<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Store extends Eloquent
{
    protected $fillable =['store_id','store_name','store_address','store_phone','store_owner'];
}
