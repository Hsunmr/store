<!DOCTYPE html>
<html lang="zh-TW">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Noto+Sans+TC&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="/css/style.css">
    <title>STORE</title>
</head>
<body>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand" href="{{route('home')}}">STORE</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
      
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav ml-auto">

            @guest
            <li class="nav-item">
              <a class="nav-link" href="{{route('login')}}">Sing In</a>
            </li>
            @else
            <li class="nav-item dropdown">

                <a id="user_dropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                    <img src="{{Auth::user()->picture}}" class="rounded" alt=""> {{ Auth::user()->name }} <span class="caret"></span>
                </a>

                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                                     document.getElementById('logout-form').submit();">
                        {{ __('Logout') }}
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </div>
            </li>
            @endguest
          </ul>
        </div>
      </nav>
    <div class="container-fluid">
        @guest
        <div id="store_header" class="container mt-5">
            <h4>請先登入會員</h4>
        </div>
        @else
        <div id="store_header" class="container mt-5">
            <h4>店家總覽</h4>
            @if ($message = Session::get('success'))
            <div class="alert alert-success" style="text-align:center;">
                <p>{{$message}}</p>
            </div>
            @endif
            <button type="button" class="btn btn-info" data-toggle="modal" data-target="#createModal">新增店家</button>
            <!-- Modal -->
            <div class="modal fade" id="createModal" tabindex="-1" role="dialog" aria-labelledby="createModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">新增店家</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="{{route('store.insert')}}" method="post" name="storeAdd" id="storeAdd">
                        @csrf
                        @method('POST')
                        <div class="form-group">
                            <label for="InputStoreName">店家名稱</label>
                            <input type="text" class="form-control" id="InputStoreName"  name="store_name" placeholder="請輸入店家名稱">
                        </div>
                        <div class="form-group">
                            <label for="InputStoreAddress">店家地址</label>
                            <input type="text" class="form-control" id="InputStoreAddress" name="store_address" placeholder="請輸入店家地址">
                        </div>
                        <div class="form-group">
                            <label for="InputStorePhone">店家電話</label>
                            <input type="text" class="form-control" id="InputStorePhone"  name="store_phone" placeholder="請輸入店家電話">
                        </div>
                        <div class="form-group">
                            <label for="InputStoreOwner">店家負責人</label>
                            <input type="text" class="form-control" id="InputStoreOwner" name="store_owner" placeholder="請輸入店家負責人">
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">新增</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
            </div>
        </div>
        <div id="store_table" class="container table-responsive">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">店家名稱</th>
                        <th scope="col">店家地址</th>
                        <th scope="col">店家電話</th>
                        <th scope="col">店家負責人</th>
                    </tr>
                </thead>
                <tbody>
                @foreach ($stores as $store)
                    <tr>
                        <td>{{$store->store_id}}</td>
                        <td>{{$store->store_name}}</td>
                        <td>{{$store->store_address}}</td>
                        <td>{{$store->store_phone}}</td>
                        <td>{{$store->store_owner}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        @endguest       
    </div>
    

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
</body>
</html>